﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Tooling.Connector;
using System.Configuration;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using System.IO;
using PIMTool.PIMToolConsoleApp;

namespace PIMToolConsoleApp
{
    class Program
    {
        static int countRecord, succesCreate, updateRecord, fail = 0;

        #region ProjectStatus
        const string CLOSED_PROJECT = "Closed";
        const string FINISHED_PROJECT = "Finished";
        const string INPROGRESS_PROJECT = "In Progress";
        const string PLANNED_PROJECT = "Planned";
        const string OPEN_PROJECT = "Open";
        #endregion

        #region ColumnName
        const int GROUP_NAME_COL = 0;
        const int PROJ_NUM_COL = 1;
        const int PROJ_NAME_COL = 2;
        const int PROJ_MEM1_COL = 3;
        const int PROJ_MEM2_COL = 4;
        const int PROJ_MEM3_COL = 5;
        const int CUST_COL = 6;
        const int PROJ_STATE_COL = 7;
        const int START_DATE_COL = 8;
        const int END_DATE_COL = 9;
        #endregion

        static Dictionary<string, int> projStateDict = new Dictionary<string, int>();

        static void ImportProject(PIMToolServiceContext context, string[] values, IOrganizationService orgservice)
        {
            var group = context.elca_ProjectGroupSet.Where(g => g.elca_name.Equals(values[GROUP_NAME_COL])).FirstOrDefault();                                                  
            string[] subStrArr = { values[PROJ_MEM1_COL].Replace("\"", ""), values[PROJ_MEM2_COL], values[PROJ_MEM3_COL].Replace("\"", "") };
            var checkExist = context.elca_ProjectSet.Where(p => p.elca_ProjectNumber.Equals(values[PROJ_NUM_COL])).FirstOrDefault();
            var project = new elca_Project();
            if (checkExist != null)
            {
                var entity = new Entity("elca_project", "elca_projectnumber", values[PROJ_NUM_COL]);
                project = entity.ToEntity<elca_Project>();
            }
            else
            {
                project.elca_ProjectNumber = values[PROJ_NUM_COL];
            }
            
            project.elca_ProjectGroupId = group != null ? group.ToEntityReference() : null;
            project.elca_ProjectNumber = values[PROJ_NUM_COL];
            project.elca_name = values[PROJ_NAME_COL];
            project.elca_Members = string.Join(",", subStrArr);
            project.elca_Customer = values[CUST_COL];
            project.elca_ProjectStatus = new OptionSetValue(projStateDict[values[PROJ_STATE_COL]]);
            project.elca_StartDate = DateTime.Parse(values[START_DATE_COL]);
            project.elca_EndDate = DateTime.Parse(values[END_DATE_COL]);
            try
            {
                orgservice.Create(project);
                succesCreate += 1;
            }
            catch (Exception e)
            {
                try
                {
                    orgservice.Update(project);
                    updateRecord += 1;

                }
                catch (Exception ex)
                {
                    fail += 1;
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.StackTrace);
                }
            }
            
        }

        static void Main(string[] args)
        {
            var con = ConfigurationManager.ConnectionStrings["CRMOnPremise"].ConnectionString;
            CrmServiceClient crmService = new CrmServiceClient(ConfigurationManager.ConnectionStrings["CRMOnPremise"].ConnectionString);
            string path = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            projStateDict.Add(CLOSED_PROJECT, (int)ProjectStatus.Closed);
            projStateDict.Add(FINISHED_PROJECT, (int)ProjectStatus.Finished);
            projStateDict.Add(INPROGRESS_PROJECT, (int)ProjectStatus.InProgress);
            projStateDict.Add(OPEN_PROJECT, (int)ProjectStatus.New);
            projStateDict.Add(PLANNED_PROJECT, (int)ProjectStatus.Planned);
            using (var reader = new StreamReader(@"D:\PIMTool\PIMToolConsoleApp\projectsampledata.csv"))
            {
                IOrganizationService orgservice = (IOrganizationService)crmService.OrganizationWebProxyClient ?? crmService.OrganizationServiceProxy;
                PIMToolServiceContext context = new PIMToolServiceContext(orgservice);
                List<elca_Project> projects = new List<elca_Project>();
                reader.ReadLine();                
                while (!reader.EndOfStream)
                {
                    countRecord += 1;
                    var line = reader.ReadLine();
                    var values = line.Split(',');                    
                    ImportProject(context, values, orgservice);
                    
                }                      
                context.SaveChanges();
                Console.WriteLine("Total records input: {0}", countRecord);
                Console.WriteLine("Success create: {0}", succesCreate);
                Console.WriteLine("Success update: {0}", updateRecord);
                Console.WriteLine("Fail: {0}", fail);
                Console.WriteLine("Import Complete!");
                Console.ReadLine();
            }
        }
    }
}
