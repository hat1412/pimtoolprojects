﻿// -----------------------------------------------------------------------
// <copyright file="ProjectDeleteException.cs" company="ELCA">
//      Copyright (c) ELCA Informatique SA Microsoft Corporation. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace PIMTool.PIMToolPlugin.Exception
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ProjectDeleteException: Exception
    {
        public ProjectDeleteException()
        {

        }

        public ProjectDeleteException(string Name)
            : base(String.Format("Deletion denies due to it status - {0}!", Name))
        {

        }
        
    }
}