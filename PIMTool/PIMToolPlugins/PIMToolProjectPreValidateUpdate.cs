// <copyright file="PreValidateProjectUpdate.cs" company="">
// Copyright (c) 2020 All Rights Reserved
// </copyright>
// <author></author>
// <date>6/19/2020 3:58:54 PM</date>
// <summary>Implements the PreValidateProjectUpdate Plugin.</summary>
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1
// </auto-generated>
namespace PIMTool.PIMToolPlugins
{
    using System;
    using System.ServiceModel;
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Query;
    using Microsoft.Xrm.Sdk.Discovery;
    using System.Linq;
    using System.Collections.Generic;

    /// <summary>
    /// PreValidateProjectUpdate Plugin.
    /// Fires when the following attributes are updated:
    /// All Attributes
    /// </summary>    
    public class PIMToolProjectPreValidateUpdate: Plugin
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PIMToolProjectPreValidateUpdate"/> class.
        /// </summary>
        public PIMToolProjectPreValidateUpdate()
            : base(typeof(PIMToolProjectPreValidateUpdate))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(10, "Update", "elca_project", new Action<LocalPluginContext>(ExecutePreValidateProjectUpdate)));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        /// Executes the plug-in.
        /// </summary>
        /// <param name="localContext">The <see cref="LocalPluginContext"/> which contains the
        /// <see cref="IPluginExecutionContext"/>,
        /// <see cref="IOrganizationService"/>
        /// and <see cref="ITracingService"/>
        /// </param>
        /// <remarks>
        /// For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        /// The plug-in's Execute method should be written to be stateless as the constructor
        /// is not called for every invocation of the plug-in. Also, multiple system threads
        /// could execute the plug-in at the same time. All per invocation state information
        /// is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecutePreValidateProjectUpdate(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }
            
            var entity = (Entity)localContext.PluginExecutionContext.InputParameters["Target"];
            var project = new elca_Project();                        
            if (entity.LogicalName != elca_Project.EntityLogicalName)
            {
                return;
            }
            if (entity.Contains("elca_members"))
            {
                project = entity.ToEntity<elca_Project>();                
                string[] arrayStr = project.elca_Members.Replace(" ", "").Split(',');
                var notFound = string.Empty;
                var orgservice = localContext.OrganizationService;
                var ptContext = new PIMToolServiceContext(orgservice);
                var relationship = new Relationship("elca_project_contact");
                if (localContext.PluginExecutionContext.InputParameters.Contains("Relationship"))
                {
                    relationship = ((Relationship)localContext.PluginExecutionContext.InputParameters["Relationship"]);
                }
                var contactReferences = new EntityReferenceCollection();
                var oldContactReferences = new EntityReferenceCollection();
                var relEnt = ptContext.elca_project_contactSet.ToList();
                
                var query = new QueryExpression
                {
                    EntityName = elca_project_contact.EntityLogicalName,
                    ColumnSet = new ColumnSet(true),
                    Criteria = new FilterExpression
                    {
                        Conditions =
                    {
                        new ConditionExpression{
                            AttributeName = "elca_projectid",
                            Operator = ConditionOperator.Equal,
                            Values = {project.Id}
                        }
                    }
                    }
                };
                var contacts = orgservice.RetrieveMultiple(query).Entities;
                foreach (var epc in contacts)
                {
                    var contact = ptContext.ContactSet.Where(c => c.Id.Equals(epc.Attributes["contactid"])).FirstOrDefault();
                    if (contact != null)
                    {
                        contactReferences.Add(contact.ToEntityReference());
                    }
                }
                orgservice.Disassociate(elca_Project.EntityLogicalName, project.Id, relationship, contactReferences);
                contactReferences = new EntityReferenceCollection();


                ptContext.ContactSet.ToList().ForEach(x =>
                {
                    if (arrayStr.Contains(x.elca_Visa))
                    {
                        contactReferences.Add(x.ToEntityReference());
                        arrayStr[arrayStr.ToList().IndexOf(x.elca_Visa)] = null;
                    }
                });
                orgservice.Associate(elca_Project.EntityLogicalName, project.Id, relationship, contactReferences);
                arrayStr.ToList().ForEach(x =>
                {
                    if (x != null)
                    {
                        notFound += x + " ";
                    }
                });

                if (notFound != string.Empty)
                {
                    ITracingService traceSvc = (ITracingService)localContext.TracingService;
                    traceSvc.Trace("Visas: {0} not found!", notFound);
                }   
            }
        }        
    }
}
