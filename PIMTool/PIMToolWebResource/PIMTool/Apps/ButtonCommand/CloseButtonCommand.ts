export class CloseButtonCommand {
    closeProject() {
        var Page = <Form.elca_project.Main.Information>Xrm.Page;
        Xrm.Utility.confirmDialog("Are you sure to close the project", function () { 
            Page.getAttribute("elca_projectstatus").setValue(elca_projectstatus.Closed) 
        }, null);
        Page.data.entity.save();
    }
}