export class EnableCloseProjectRule {
    checkCondition(): boolean{    
        var Page = <Form.elca_project.Main.Information> Xrm.Page;        
        var projectState = Page.getAttribute("elca_projectstatus").getValue();
        if(projectState != null){
            var now = new Date().getTime();
            var endDate = Page.getAttribute("elca_enddate").getValue().getTime();                    
            if(projectState == elca_projectstatus.Closed || endDate == null || (now-endDate) < 30) {                
                return false;                
            }
            else {
                return false;
            }
        }        
        return true;
    }
}