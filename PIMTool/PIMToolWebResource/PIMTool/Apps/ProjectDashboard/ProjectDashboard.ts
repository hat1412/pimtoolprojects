import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-select';
import 'datatables.net-dt';

export class DashboardTable {    

    createTable() {        
        $(document).ready(function () {
            var tableData = [];
            XrmQuery.retrieveMultiple(x => x.elca_projects).orderAsc(x => x.elca_projectnumber).execute(x => {
                console.log(x);
                var t = $('#projectTable').DataTable({
                    columnDefs: [{
                        orderable: false,
                        className: 'select-checkbox',
                        targets: 0
                    }],
                    select: {
                        style: 'os',
                        selector: 'td:first-child'
                    },
                    order: [[1, 'asc']]
                });



                x.forEach(project => {
                    var status = "";
                    var visible = "";
                    switch (project.elca_projectstatus) {
                        case elca_projectstatus.Closed:
                            status = "Closed";
                            visible = "hidden";
                            break;
                        case elca_projectstatus.Finished:
                            status = "Finished";
                            visible = "hidden";
                            break;
                        case elca_projectstatus.InProgress:
                            status = "In Progress";
                            visible = "hidden";
                            break;
                        case elca_projectstatus.New:
                            status = "New";
                            visible = "visible";
                            break;
                        case elca_projectstatus.Planned:
                            status = "Planned";
                            visible = "hidden";
                            break;
                    }
                    t.row.add(
                        [
                            '',
                            '<a href="http://peik-crm:5555/TEST/main.aspx?etn=elca_project&pagetype=entityrecord&id=%7B' + project.elca_projectid + '%7D" target="_blank">' + project.elca_projectnumber + '</a>',
                            project.elca_name,
                            status,
                            project.elca_customer,
                            new Intl.DateTimeFormat('en-GB').format(project.elca_startdate),
                            '<a href="#" data-project-id="xxx"  id="btn-delete" style="visibility:' + visible + ';" data-toggle="modal" data-target="#deleteConfirm"><img src="./trash.png" /></a>'
						    + '<input type="hidden" value="'+ project.elca_projectid +'" id="#deleteitemid"/>',
                        ]

                        ,
                        
                    ).draw(false);
                });
            });
        });


    }

    filterTable() {
        $('#searchterm').on('keyup', function () {
            if ($('#searchterm').val() != "" && $("#searchterm").val() != null) {
                var searchterm = $("#searchterm").val();
                $('#projectTable').DataTable().search(searchterm.toString()).draw();
            }
        });
        $('#search').on('click', function () {
            var searchterm = $("#searchterm").val();
            var criteria = $('#criteria').val();
            switch (parseInt(criteria.toString())) {
                case 1:
                    $('#projectTable').DataTable().columns(2).search(searchterm.toString()).draw();
                    break;
                case 2:
                    $('#projectTable').DataTable().columns(1).search(searchterm.toString()).draw();
                    break;
                case 3:
                    $('#projectTable').DataTable().columns(4).search(searchterm.toString()).draw();
                    break;
                case 4:
                    $('#projectTable').DataTable().columns(3).search(searchterm.toString()).draw();
                    break;
            }
        });
        $('#reset').on('click', function () {
            $('#projectTable').DataTable().search("").draw();
            $('#projectTable').DataTable().columns(1).search("").draw();
            $('#projectTable').DataTable().columns(2).search("").draw();
            $('#projectTable').DataTable().columns(3).search("").draw();
            $('#projectTable').DataTable().columns(4).search("").draw();
            $('#searchterm').val("");
            $('#criteria').val(1);
        });
    }

    rowSelect() {
        $("#projectTable tbody").on('click', 'tr', function () {
            $(this).toggleClass('selected');
            var count = 0;
            var table = $('#projectTable').DataTable();
            count = table.rows('.selected').data().length;
            var pTag = "";
            var aTag = "";
            if (count > 0) {
                pTag = '<span class="text-left float-left" style="visibility:visible;">' + count + ' items selected</span>';
                aTag = '<a class="text-right float-right" href="#" id="btn-confirm" style="visibility:visible;" data-toggle="modal" data-target="#deleteConfirm">Delete selected items<img src="./trash.png" style="width:40; height40;/></a>'
                $('#footer').html(pTag + aTag);
            }
            else {
                pTag = '<span class="text-left float-left" style="visibility:hidden;">' + count + ' items selected</p>';
                aTag = '<a class="text-right float-right" href="#" id="btn-confirm" style="visibility:hidden;" data-toggle="modal" data-target="#deleteConfirm">Delete selected items<img src="./trash.png" style="width:40; height40;/></a>'
                $('#footer').html(pTag + aTag);
            }
        });
    }

    deleteItems() {
        var iddelete = null;
        $('#btn-delete').on('click', function(clickedElement){            
            iddelete = $(clickedElement).attr('data-project-id');
            var t = confirm("Are you sure delete the project?");
            if(t === true){
                XrmQuery.deleteRecord(x => x.elca_projects, iddelete).execute(
                    success => {
                        console.log(success);
                        location.reload();
                    }, 
                    error=>{
                        console.log(error);
                        alert(error.message);
                        console.log(error)
                    });
            }            
        });        
    }

}