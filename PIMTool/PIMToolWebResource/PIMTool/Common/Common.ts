﻿export namespace CrmHelper {
    interface HttpRequest {
        method: string;
        url: string;
        header: object;
        data: object;
        async: boolean;
        successCallback: Function;
        errorCallback: Function;
    };

    export const disableFormFields = (): void => {
        Xrm.Page.ui.controls.forEach((control, idx) => {
            if (isControlHaveAttribute(control)) {
                const attr = control.getAttribute();
                if (typeof (attr) !== "undefined") {
                    control.setDisabled(true);
                }
            }
        });
    };

    export const isControlHaveAttribute = (control: Xrm.AnyControl): boolean => {
        const controlType = control.getControlType();
        return ["iframe", "webresource", "subgrid"].indexOf(controlType) === -1;
    };
}