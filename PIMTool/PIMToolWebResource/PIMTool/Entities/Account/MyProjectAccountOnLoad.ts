﻿import { CrmHelper } from "../../Common/Common";

export class MyProjectAccount {
    static onLoadHandler(): void {
        // This code is for CRM v8.x
        if (Xrm.Page.ui.getFormType() === Xrm.FormType.Create) {
            CrmHelper.disableFormFields();
            Xrm.Utility.alertDialog("Disabled form account quick create xxxx.");
        }
    }
}
