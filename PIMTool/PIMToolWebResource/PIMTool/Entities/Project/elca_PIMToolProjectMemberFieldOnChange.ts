export class MemberFieldOnChange {
    fieldValidate() {
        debugger;
        var Page = <Form.elca_project.Main.Information>Xrm.Page;
        var members = Page.getAttribute("elca_members").getValue().replace(" ", "");
        if (members != null && members != "") {
            Page.getControl("elca_members").clearNotification();            
            var memVisas = members.split(',');
            var notFound = "";
            // var empGrid = Page.getControl("Member");
            memVisas.forEach(visa => {
                XrmQuery.retrieveMultiple(x => x.contacts)
                    .filter(x => Filter.equals(x.elca_visa, visa))
                    .execute(contact => {
                        if (contact.length > 0) {
                            console.log(contact.pop());
                        }                        
                        else {
                            notFound += visa + " ";
                            if (memVisas.indexOf(visa) == (memVisas.length - 1)) {
                                Page.getControl("elca_members").setNotification(notFound + "is not found!", "ERROR");
                            }
                            if(memVisas.indexOf(visa, 0) > -1){
                                memVisas.splice(memVisas.indexOf(visa, 0), 1);
                            }
                        }

                        if (memVisas.indexOf(visa) == (memVisas.length - 1)) {
                            members = memVisas.join(',');
                            Page.getAttribute("elca_members").setValue(members);                    
                        }
                    });                
            });                
            
        }
    }
}