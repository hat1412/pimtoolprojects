﻿export class ProjectOnLoad {
    editFormChecking() {
        var Page = <Form.elca_project.Main.Information> Xrm.Page;
        var formType = Page.ui.getFormType();
        if(formType == 2) {
            var fieldObj = Page.getAttribute("elca_projectnumber");
            if(fieldObj.getValue() != null){
                Page.getControl("elca_projectnumber").setDisabled(true);
                Page.getControl("elca_projectstatus").setDisabled(false);
            }
            else{
                Page.getControl("elca_projectstatus").setDisabled(true);
            }
        }
    }
}    