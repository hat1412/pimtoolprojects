interface elca_ProjectGroup_Base extends WebEntity {
  createdon?: Date | null;
  elca_name?: string | null;
  elca_projectgroupid?: string | null;
  importsequencenumber?: number | null;
  modifiedon?: Date | null;
  overriddencreatedon?: Date | null;
  statecode?: elca_projectgroup_statecode | null;
  statuscode?: elca_projectgroup_statuscode | null;
  timezoneruleversionnumber?: number | null;
  utcconversiontimezonecode?: number | null;
  versionnumber?: number | null;
}
interface elca_ProjectGroup_Relationships {
  elca_GroupLeaderId?: Contact_Result | null;
  elca_elca_projectgroup_elca_project?: elca_Project_Result[] | null;
}
interface elca_ProjectGroup extends elca_ProjectGroup_Base, elca_ProjectGroup_Relationships {
  elca_GroupLeaderId_bind$contacts?: string | null;
  ownerid_bind$systemusers?: string | null;
  ownerid_bind$teams?: string | null;
}
interface elca_ProjectGroup_Create extends elca_ProjectGroup {
}
interface elca_ProjectGroup_Update extends elca_ProjectGroup {
}
interface elca_ProjectGroup_Select {
  createdby_guid: WebAttribute<elca_ProjectGroup_Select, { createdby_guid: string | null }, { createdby_formatted?: string }>;
  createdon: WebAttribute<elca_ProjectGroup_Select, { createdon: Date | null }, { createdon_formatted?: string }>;
  createdonbehalfby_guid: WebAttribute<elca_ProjectGroup_Select, { createdonbehalfby_guid: string | null }, { createdonbehalfby_formatted?: string }>;
  elca_groupleaderid_guid: WebAttribute<elca_ProjectGroup_Select, { elca_groupleaderid_guid: string | null }, { elca_groupleaderid_formatted?: string }>;
  elca_name: WebAttribute<elca_ProjectGroup_Select, { elca_name: string | null }, {  }>;
  elca_projectgroupid: WebAttribute<elca_ProjectGroup_Select, { elca_projectgroupid: string | null }, {  }>;
  importsequencenumber: WebAttribute<elca_ProjectGroup_Select, { importsequencenumber: number | null }, {  }>;
  modifiedby_guid: WebAttribute<elca_ProjectGroup_Select, { modifiedby_guid: string | null }, { modifiedby_formatted?: string }>;
  modifiedon: WebAttribute<elca_ProjectGroup_Select, { modifiedon: Date | null }, { modifiedon_formatted?: string }>;
  modifiedonbehalfby_guid: WebAttribute<elca_ProjectGroup_Select, { modifiedonbehalfby_guid: string | null }, { modifiedonbehalfby_formatted?: string }>;
  overriddencreatedon: WebAttribute<elca_ProjectGroup_Select, { overriddencreatedon: Date | null }, { overriddencreatedon_formatted?: string }>;
  ownerid_guid: WebAttribute<elca_ProjectGroup_Select, { ownerid_guid: string | null }, { ownerid_formatted?: string }>;
  owningbusinessunit_guid: WebAttribute<elca_ProjectGroup_Select, { owningbusinessunit_guid: string | null }, { owningbusinessunit_formatted?: string }>;
  owningteam_guid: WebAttribute<elca_ProjectGroup_Select, { owningteam_guid: string | null }, { owningteam_formatted?: string }>;
  owninguser_guid: WebAttribute<elca_ProjectGroup_Select, { owninguser_guid: string | null }, { owninguser_formatted?: string }>;
  statecode: WebAttribute<elca_ProjectGroup_Select, { statecode: elca_projectgroup_statecode | null }, { statecode_formatted?: string }>;
  statuscode: WebAttribute<elca_ProjectGroup_Select, { statuscode: elca_projectgroup_statuscode | null }, { statuscode_formatted?: string }>;
  timezoneruleversionnumber: WebAttribute<elca_ProjectGroup_Select, { timezoneruleversionnumber: number | null }, {  }>;
  utcconversiontimezonecode: WebAttribute<elca_ProjectGroup_Select, { utcconversiontimezonecode: number | null }, {  }>;
  versionnumber: WebAttribute<elca_ProjectGroup_Select, { versionnumber: number | null }, {  }>;
}
interface elca_ProjectGroup_Filter {
  createdby_guid: XQW.Guid;
  createdon: Date;
  createdonbehalfby_guid: XQW.Guid;
  elca_groupleaderid_guid: XQW.Guid;
  elca_name: string;
  elca_projectgroupid: XQW.Guid;
  importsequencenumber: number;
  modifiedby_guid: XQW.Guid;
  modifiedon: Date;
  modifiedonbehalfby_guid: XQW.Guid;
  overriddencreatedon: Date;
  ownerid_guid: XQW.Guid;
  owningbusinessunit_guid: XQW.Guid;
  owningteam_guid: XQW.Guid;
  owninguser_guid: XQW.Guid;
  statecode: elca_projectgroup_statecode;
  statuscode: elca_projectgroup_statuscode;
  timezoneruleversionnumber: number;
  utcconversiontimezonecode: number;
  versionnumber: number;
}
interface elca_ProjectGroup_Expand {
  elca_GroupLeaderId: WebExpand<elca_ProjectGroup_Expand, Contact_Select, Contact_Filter, { elca_GroupLeaderId: Contact_Result }>;
  elca_elca_projectgroup_elca_project: WebExpand<elca_ProjectGroup_Expand, elca_Project_Select, elca_Project_Filter, { elca_elca_projectgroup_elca_project: elca_Project_Result[] }>;
}
interface elca_ProjectGroup_FormattedResult {
  createdby_formatted?: string;
  createdon_formatted?: string;
  createdonbehalfby_formatted?: string;
  elca_groupleaderid_formatted?: string;
  modifiedby_formatted?: string;
  modifiedon_formatted?: string;
  modifiedonbehalfby_formatted?: string;
  overriddencreatedon_formatted?: string;
  ownerid_formatted?: string;
  owningbusinessunit_formatted?: string;
  owningteam_formatted?: string;
  owninguser_formatted?: string;
  statecode_formatted?: string;
  statuscode_formatted?: string;
}
interface elca_ProjectGroup_Result extends elca_ProjectGroup_Base, elca_ProjectGroup_Relationships {
  "@odata.etag": string;
  createdby_guid: string | null;
  createdonbehalfby_guid: string | null;
  elca_groupleaderid_guid: string | null;
  modifiedby_guid: string | null;
  modifiedonbehalfby_guid: string | null;
  ownerid_guid: string | null;
  owningbusinessunit_guid: string | null;
  owningteam_guid: string | null;
  owninguser_guid: string | null;
}
interface elca_ProjectGroup_RelatedOne {
  elca_GroupLeaderId: WebMappingRetrieve<Contact_Select,Contact_Expand,Contact_Filter,Contact_Fixed,Contact_Result,Contact_FormattedResult>;
}
interface elca_ProjectGroup_RelatedMany {
  elca_elca_projectgroup_elca_project: WebMappingRetrieve<elca_Project_Select,elca_Project_Expand,elca_Project_Filter,elca_Project_Fixed,elca_Project_Result,elca_Project_FormattedResult>;
}
interface WebEntitiesRetrieve {
  elca_projectgroups: WebMappingRetrieve<elca_ProjectGroup_Select,elca_ProjectGroup_Expand,elca_ProjectGroup_Filter,elca_ProjectGroup_Fixed,elca_ProjectGroup_Result,elca_ProjectGroup_FormattedResult>;
}
interface WebEntitiesRelated {
  elca_projectgroups: WebMappingRelated<elca_ProjectGroup_RelatedOne,elca_ProjectGroup_RelatedMany>;
}
interface WebEntitiesCUDA {
  elca_projectgroups: WebMappingCUDA<elca_ProjectGroup_Create,elca_ProjectGroup_Update,elca_ProjectGroup_Select>;
}
