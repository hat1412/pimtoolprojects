interface WebMappingRetrieve<ISelect, IExpand, IFilter, IFixed, Result, FormattedResult> {
}
interface WebMappingCUDA<ICreate, IUpdate, ISelect> {
}
interface WebMappingRelated<ISingle, IMultiple> {
}
interface WebEntity {
}
interface WebEntity_Fixed {
  "@odata.etag": string;
}
interface Contact_Base extends WebEntity {
}
interface Contact_Fixed extends WebEntity_Fixed {
  contactid: string;
}
interface Contact extends Contact_Base, Contact_Relationships {
}
interface Contact_Relationships {
}
interface Contact_Result extends Contact_Base, Contact_Relationships {
}
interface Contact_FormattedResult {
}
interface Contact_Select {
}
interface Contact_Expand {
}
interface Contact_Filter {
}
interface Contact_Create extends Contact {
}
interface Contact_Update extends Contact {
}
interface elca_Project_Base extends WebEntity {
}
interface elca_Project_Fixed extends WebEntity_Fixed {
  elca_projectid: string;
}
interface elca_Project extends elca_Project_Base, elca_Project_Relationships {
}
interface elca_Project_Relationships {
}
interface elca_Project_Result extends elca_Project_Base, elca_Project_Relationships {
}
interface elca_Project_FormattedResult {
}
interface elca_Project_Select {
}
interface elca_Project_Expand {
}
interface elca_Project_Filter {
}
interface elca_Project_Create extends elca_Project {
}
interface elca_Project_Update extends elca_Project {
}
interface elca_project_contact_Base extends WebEntity {
}
interface elca_project_contact_Fixed extends WebEntity_Fixed {
  elca_project_contactid: string;
}
interface elca_project_contact extends elca_project_contact_Base, elca_project_contact_Relationships {
}
interface elca_project_contact_Relationships {
}
interface elca_project_contact_Result extends elca_project_contact_Base, elca_project_contact_Relationships {
}
interface elca_project_contact_FormattedResult {
}
interface elca_project_contact_Select {
}
interface elca_project_contact_Expand {
}
interface elca_project_contact_Filter {
}
interface elca_project_contact_Create extends elca_project_contact {
}
interface elca_project_contact_Update extends elca_project_contact {
}
interface elca_ProjectGroup_Base extends WebEntity {
}
interface elca_ProjectGroup_Fixed extends WebEntity_Fixed {
  elca_projectgroupid: string;
}
interface elca_ProjectGroup extends elca_ProjectGroup_Base, elca_ProjectGroup_Relationships {
}
interface elca_ProjectGroup_Relationships {
}
interface elca_ProjectGroup_Result extends elca_ProjectGroup_Base, elca_ProjectGroup_Relationships {
}
interface elca_ProjectGroup_FormattedResult {
}
interface elca_ProjectGroup_Select {
}
interface elca_ProjectGroup_Expand {
}
interface elca_ProjectGroup_Filter {
}
interface elca_ProjectGroup_Create extends elca_ProjectGroup {
}
interface elca_ProjectGroup_Update extends elca_ProjectGroup {
}
interface ActivityParty_Base extends WebEntity {
}
interface ActivityParty_Fixed extends WebEntity_Fixed {
  activitypartyid: string;
}
interface ActivityParty extends ActivityParty_Base, ActivityParty_Relationships {
}
interface ActivityParty_Relationships {
}
interface ActivityParty_Result extends ActivityParty_Base, ActivityParty_Relationships {
}
interface ActivityParty_FormattedResult {
}
interface ActivityParty_Select {
}
interface ActivityParty_Expand {
}
interface ActivityParty_Filter {
}
interface ActivityParty_Create extends ActivityParty {
}
interface ActivityParty_Update extends ActivityParty {
}
interface Connection_Base extends WebEntity {
}
interface Connection_Fixed extends WebEntity_Fixed {
  connectionid: string;
}
interface Connection extends Connection_Base, Connection_Relationships {
}
interface Connection_Relationships {
}
interface Connection_Result extends Connection_Base, Connection_Relationships {
}
interface Connection_FormattedResult {
}
interface Connection_Select {
}
interface Connection_Expand {
}
interface Connection_Filter {
}
interface Connection_Create extends Connection {
}
interface Connection_Update extends Connection {
}
interface PostFollow_Base extends WebEntity {
}
interface PostFollow_Fixed extends WebEntity_Fixed {
  postfollowid: string;
}
interface PostFollow extends PostFollow_Base, PostFollow_Relationships {
}
interface PostFollow_Relationships {
}
interface PostFollow_Result extends PostFollow_Base, PostFollow_Relationships {
}
interface PostFollow_FormattedResult {
}
interface PostFollow_Select {
}
interface PostFollow_Expand {
}
interface PostFollow_Filter {
}
interface PostFollow_Create extends PostFollow {
}
interface PostFollow_Update extends PostFollow {
}
