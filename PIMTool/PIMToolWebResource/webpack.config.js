﻿const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
var config = {
    resolve: {
        extensions: ['.ts', '.js', '.css']
    },
    module: {
        rules: [{
            test: /\.ts$/,
            loader: 'awesome-typescript-loader',
            exclude: /node_modules/,
        },
        {
            test: /\.(sa|sc|c)ss$/,
            use: [{
                loader: MiniCssExtractPlugin.loader,
                options: {
                    hmr: process.env.NODE_ENV === 'development',
                },
            },
            {
                loader: 'css-loader',
                options: {
                    url: false
                }
            },
                'sass-loader'
            ],
            exclude: /\.svg$/
        }
        ]
    },
    mode: 'develpment',
    devtool: 'sourcemap'
}

var eventCalendarConfig = Object.assign({}, config, {
    entry: {
        "js/CloseProjectCommandRule": path.join(__dirname, "PIMTool/Apps/CommandRule/CloseProjectCommandRule.ts"),
        "CloseProjectCommandRule": path.join(__dirname, "PIMTool/Apps/CommandRule/CloseProjectCommandRule.ts"),

        "js/CloseButtonCommand": path.join(__dirname, "PIMTool/Apps/ButtonCommand/CloseButtonCommand.ts"),
        "CloseButtonCommand.ts": path.join(__dirname, "PIMTool/Apps/ButtonCommand/CloseButtonCommand.ts"),

        "js/elca_PIMToolProjectOnLoad": path.join(__dirname, "PIMTool/Entities/Project/elca_PIMToolProjectOnLoad.ts"),
        "elca_PIMToolProjectOnLoad": path.join(__dirname, "PIMTool/Entities/Project/elca_PIMToolProjectOnLoad.ts"),

        "js/elca_PIMToolProjectMemberFieldOnChange": path.join(__dirname, "PIMTool/Entities/Project/elca_PIMToolProjectMemberFieldOnChange.ts"),
        "elca_PIMToolProjectMemberFieldOnChange": path.join(__dirname, "PIMTool/Entities/Project/elca_PIMToolProjectMemberFieldOnChange.ts"),

        "js/ProjectDashboard": path.join(__dirname, "PIMTool/Apps/ProjectDashboard/ProjectDashboard.ts"),
        "ProjectDashboard": path.join(__dirname, "PIMTool/Apps/ProjectDashboard/ProjectDashboard.ts"),

        "Apps/ProjectDashboard/ProjectDashboard": path.join(__dirname, "PIMTool/Apps/ProjectDashboard/ProjectDashboard.scss"),
    },
    output: {
        path: path.join(__dirname, "PIMTool"),
        filename: '[name].js',
        library: 'PIMTool_[name]_lib',
        libraryTarget: 'var',
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename:'[name].css'
        }),
    ]
})

module.exports = [
    eventCalendarConfig
]