﻿using System;
using FakeXrmEasy;
using System.Linq;
using Microsoft.Xrm.Sdk;
using NUnit.Framework;
using PIMTool.PIMToolPlugins;

namespace PreProjectDeleteUnitTest
{
    [TestFixture]
    public class UnitTest1
    {
        [Test]
        public void TestMethod1()
        {
            var fakeContext = new XrmFakedContext();
            var iTracing = new XrmFakedTracingService();            
            var ptcontext = new PIMToolServiceContext(fakeContext.GetFakedOrganizationService());

            var projectTarget = ptcontext.elca_ProjectSet.First();
            var inputParam = new ParameterCollection();
            inputParam.Add("Target", projectTarget);
            var plugCtx = fakeContext.GetDefaultPluginContext();
            plugCtx.InputParameters = inputParam;           
            //fakeContext.Initialize(new System.Collections.Generic.List<)
            Assert.Throws(typeof(InvalidPluginExecutionException), () => fakeContext.ExecutePluginWith<ProjectPreValidateDelete>(plugCtx));
            Assert.DoesNotThrow(() => fakeContext.ExecutePluginWith<ProjectPreValidateDelete>(plugCtx));
            iTracing.Trace("Tested Trace in Account Create");
        }
    }
}
